Jinja2==3.0.3
werkzeug==2.0.2
itsdangerous==2.0.1
Flask==1.1.2
Flask-Migrate==2.5.3
SQLAlchemy<1.4.0
gunicorn==20.0.4
