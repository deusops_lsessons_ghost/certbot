output "internal_ip_web_server" {
  value = yandex_compute_instance.web.*.network_interface.0.ip_address
}

output "internal_ip_nginx_server" {
  value = yandex_compute_instance.nginx.*.network_interface.0.ip_address
}

output "external_ip_web_server" {
  value = yandex_compute_instance.web.*.network_interface.0.nat_ip_address
}

output "external_ip_nginx_server" {
  value = yandex_compute_instance.nginx.*.network_interface.0.nat_ip_address
}


resource "local_file" "ansible_hosts" {
  content    = templatefile("./template/hosts.tpl",
   {
    domain_name     = var.domain-name,

    web_user        = var.web-ssh-user,
    web_ssh         = var.web-ssh-key-path,
    web_host        = var.web-instance-name,
    web_inner_ip    = "${yandex_compute_instance.web.*.network_interface.0.ip_address}",
    web_external_ip = "${yandex_compute_instance.web.*.network_interface.0.nat_ip_address}"

    nginx_user        = var.nginx-ssh-user,
    nginx_ssh         = var.nginx-ssh-key-path,
    nginx_host        = var.nginx-instance-name,
    nginx_inner_ip    = "${yandex_compute_instance.nginx.*.network_interface.0.ip_address}",
    nginx_external_ip = "${yandex_compute_instance.nginx.*.network_interface.0.nat_ip_address}"
  }
)
filename = "../hosts"
}
