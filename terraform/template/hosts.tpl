[web_srv]
%{ for index, ip in web_external_ip ~}
${web_host}${index + 1} ansible_host=${ip} ansible_user=${web_user} ansible_ssh_private_key_file=${web_ssh}
%{ endfor ~}

[nginx_srv]
%{ for index, ip in nginx_external_ip ~}
${nginx_host}${index + 1} ansible_host=${ip} ansible_user=${nginx_user} ansible_ssh_private_key_file=${nginx_ssh}
%{ endfor ~}

[all:children]
web_srv
nginx_srv

[all:vars]
%{ for index, ip in web_inner_ip ~}
web_in_ip${index + 1} = ${ip}
%{ endfor ~}
%{ for index, ip in nginx_inner_ip ~}
nginx_in_ip${index + 1} = ${ip}
%{ endfor ~}
d_name = ${domain_name}
