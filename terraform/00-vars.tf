#============================= Yandex Cloud variables ==============================#

variable "zone" {
  description = "Deafult zone"
  default     = "ru-central1-a"
}

variable "yandex-token" {
  description = "Yandex token"
}

variable "yandex-cloud-id" {
  description = "Yandex cloud id"
}

variable "yandex-folder-id" {
  description = "Yandex folder id"
}

variable "yandex-service-account-id" {
  description = "Yandex service account id"
}

#================================ DNS variables ===================================#

variable "domain-name" {
  description = "Domain name"
  type        = string
  default     = "deusopsghost.ml"
}

variable "domain-name-prefix-web" {
  description = "Prefix for web app servers"
  type        = string
  default     = "web"
}


#=============================== Instance variables ==================================#

variable "instance-platform-id" {
  description = "Type of instance CPUs"
  type        = string
  default     = "standard-v3"
}

variable "instance-cores" {
  description = "Amount of CPU cores"
  type        = number
  default     = 2
}

variable "instance-memory" {
  description = "Amount of memory, GB"
  type        = number
  default     = 1
}

variable "instance-core-fraction" {
  description = "Fraction per core, %"
  type        = number
  default     = 20
}

variable "instance-image-id" {
  # Ubuntu 22.04 LTS => "fd8egv6phshj1f64q94n"
  # Ubuntu 20.04 LTS => "fd8kdq6d0p8sij7h5qe3"
  description = "Image for instances, Ubuntu 20.04 LTS"
  type        = string
  default     = "fd8egv6phshj1f64q94n"
}

variable "instance-disk-type" {
  description = "Type of disk (HDD, SSD)"
  type        = string
  default     = "network-hdd"
}

variable "instance-disk-size" {
  description = "Size of disk, GB"
  type        = number
  default     = 10
}

variable "is-auto-shutdown" {
  description = "Is 24h autoshutdown true"
  type        = bool
  default     = true
}

#=============================== Instances variables ==================================#


#--------------------- Web server ---------------------#

variable "web-ssh-user" {
  description = "Web instance user name"
  type        = string
  default     = "ghost_w"
}

variable "web-ssh-key-path" {
  description = "Web instance ssh public key path"
  type        = string
  default     = "~/.ssh/web_rsa"
}

variable "web-instances" {
  description = "Amount of servers to start"
  default     = 3
}

variable "web-instance-name" {
  description = "Name of web instance"
  type        = string
  default     = "web"
}

variable "web-instance-description" {
  description = "Web instance description"
  type        = string
  default     = "Instance with web app."
}

variable "web-instance-user-template" {
  description = "Creat new user on each instance"
  default     = "./template/ssh_user.tpl"
}


#--------------------- Nginx proxy server ---------------------#

variable "nginx-ssh-user" {
  description = "Nginx proxy instance user name"
  type        = string
  default     = "ghost_n"
}

variable "nginx-ssh-key-path" {
  description = "Nginx proxy instance ssh public key path"
  type        = string
  default     = "~/.ssh/proxy_rsa"
}

variable "nginx-instances" {
  description = "Amount of servers to start"
  default     = 1
}

variable "nginx-instance-name" {
  description = "Name of nginx instance"
  type        = string
  default     = "nginx"
}

variable "nginx-instance-description" {
  description = "Nginx proxy instance description"
  type        = string
  default     = "Instance with nginx proxy."
}

variable "nginx-instance-user-template" {
  description = "Creat new user on each instance"
  default     = "./template/ssh_user.tpl"
}
