Role Name
=========

That role can help you to add let's encrypt certificate for your server.

Requirements
------------

Nothing special, but correctly filled /defaults/main.yml file

Role Variables
--------------

Need to add domains to "certbot_certs"? and strongly recommend that you fill out our email correctly in "certbot_admin_email" 

Dependencies
------------

"Standalone" method has no dependencies, for "nginx" method needs nginx installed and run.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - sertbot
License
-------

Apache-2.0

Author Information
------------------

---
